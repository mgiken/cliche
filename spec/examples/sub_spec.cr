require "../spec_helper"

{{ `crystal build -o bin/sub examples/sub.cr` }}

def sub(*args)
  run("bin/sub", args)
end

describe "examples/sub" do
  context "without options and arguments" do
    it "print \"hello, main\" and exit normal" do
      o, e, r = sub

      o.should eq(<<-EOS)
        hello, main

        EOS
      e.should be_empty
      r.should eq(0)
    end
  end

  context "with `-h` option" do
    it "print usage and exit normal" do
      o, e, r = sub("-h")

      o.should eq(<<-EOS)
        Usage: sub [option] <command> [<args>]

        Main description

        Options:
            -v, --version                    print the version number and exit
            -h, --help                       print this help message and exit

        Commands:
            foo                              sub foo description

        For help on any individual command run `sub <command> --help`

        EOS
      e.should be_empty
      r.should eq(0)
    end
  end

  context "with `--help` option" do
    it "print usage and exit normal" do
      o, e, r = sub("-h")

      o.should eq(<<-EOS)
        Usage: sub [option] <command> [<args>]

        Main description

        Options:
            -v, --version                    print the version number and exit
            -h, --help                       print this help message and exit

        Commands:
            foo                              sub foo description

        For help on any individual command run `sub <command> --help`

        EOS
      e.should be_empty
      r.should eq(0)
    end
  end

  context "with `-v` option" do
    it "print version and exit normal" do
      o, e, r = sub("-v")

      o.should eq(<<-EOS)
        sub #{CLIche::VERSION}

        EOS
      e.should be_empty
      r.should eq(0)
    end
  end

  context "with `--version` option" do
    it "print version and exit normal" do
      o, e, r = sub("--version")

      o.should eq(<<-EOS)
        sub #{CLIche::VERSION}

        EOS
      e.should be_empty
      r.should eq(0)
    end
  end

  context "with `foo` sub command" do
    it "print \"hello, sub foo\" and exit normal" do
      o, e, r = sub("foo")

      o.should eq(<<-EOS)
        hello, sub foo

        EOS
      e.should be_empty
      r.should eq(0)
    end
  end

  context "with `foo` sub command and `-h` option" do
    it "print sub usage and exit normal" do
      o, e, r = sub("foo", "-h")

      o.should eq(<<-EOS)
        Usage: sub foo [option]

        Sub foo description

        Options:
            -h, --help                       print this help message and exit

        EOS
      e.should be_empty
      r.should eq(0)
    end
  end

  context "with `foo` sub command and `--help` option" do
    it "print sub usage and exit normal" do
      o, e, r = sub("foo", "--help")

      o.should eq(<<-EOS)
        Usage: sub foo [option]

        Sub foo description

        Options:
            -h, --help                       print this help message and exit

        EOS
      e.should be_empty
      r.should eq(0)
    end
  end

  context "with unrecognized option" do
    it "print unrecognized option error and exit abnormal" do
      o, e, r = sub("-x")

      o.should be_empty
      e.should eq(<<-EOS)
        sub: unrecognized option '-x'
        Try `sub --help` for more information.

        EOS
      r.should eq(1)
    end
  end

  context "with invalid argument" do
    it "print invalid argument error and exit abnormal" do
      o, e, r = sub("foo", "bar")

      o.should be_empty
      e.should eq(<<-EOS)
        sub foo: invalid argument
        Try `sub foo --help` for more information.

        EOS
      r.should eq(1)
    end
  end

  context "with unknown command" do
    it "print unknown command error and exit abnormal" do
      o, e, r = sub("bar")

      o.should be_empty
      e.should eq(<<-EOS)
        sub: unknown command 'bar'
        Try `sub --help` for more information.

        EOS
      r.should eq(1)
    end
  end
end
