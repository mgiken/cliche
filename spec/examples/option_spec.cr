require "../spec_helper"

{{ `crystal build -o bin/option examples/option.cr` }}

def option(*args)
  run("bin/option", args)
end

describe "examples/option" do
  context "without options and arguments" do
    it "print option values and exit normal" do
      o, e, r = option

      o.should eq(<<-EOS)
        a = false
        b = 1
        c = foo
        d = false
        e = false

        EOS
      e.should be_empty
      r.should eq(0)
    end
  end

  context "with valid options" do
    it "print option values and exit normal" do
      o, e, r = option("--aaa", "--bbb", "2", "--ccc", "bar")

      o.should eq(<<-EOS)
        a = true
        b = 2
        c = bar
        d = false
        e = false

        EOS
      e.should be_empty
      r.should eq(0)
    end
  end

  context "with `-h` option" do
    it "print usage and exit normal" do
      o, e, r = option("-h")

      o.should eq(<<-EOS)
        Usage: option [option]

        Options:
            -a, --aaa                        AAA
            -b BBB, --bbb=BBB                BBB
            -c, --ccc=CCC                    CCC
            -d                               DDD
            --eee                            EEE
            -v, --version                    print the version number and exit
            -h, --help                       print this help message and exit

        EOS
      e.should be_empty
      r.should eq(0)
    end
  end

  context "with `--help` option" do
    it "print usage and exit normal" do
      o, e, r = option("-h")

      o.should eq(<<-EOS)
        Usage: option [option]

        Options:
            -a, --aaa                        AAA
            -b BBB, --bbb=BBB                BBB
            -c, --ccc=CCC                    CCC
            -d                               DDD
            --eee                            EEE
            -v, --version                    print the version number and exit
            -h, --help                       print this help message and exit

        EOS
      e.should be_empty
      r.should eq(0)
    end
  end

  context "with `-v` option" do
    it "print version and exit normal" do
      o, e, r = option("-v")

      o.should eq(<<-EOS)
        option #{CLIche::VERSION}

        EOS
      e.should be_empty
      r.should eq(0)
    end
  end

  context "with `--version` option" do
    it "print version and exit normal" do
      o, e, r = option("--version")

      o.should eq(<<-EOS)
        option #{CLIche::VERSION}

        EOS
      e.should be_empty
      r.should eq(0)
    end
  end

  context "with unrecognized option" do
    it "print unrecognized option error and exit abnormal" do
      o, e, r = option("-x")

      o.should be_empty
      e.should eq(<<-EOS)
        option: unrecognized option '-x'
        Try `option --help` for more information.

        EOS
      r.should eq(1)
    end
  end

  context "with invalid argument" do
    it "print invalid argument error and exit abnormal" do
      o, e, r = option("foo")

      o.should be_empty
      e.should eq(<<-EOS)
        option: invalid argument
        Try `option --help` for more information.

        EOS
      r.should eq(1)
    end
  end

  context "with missing option" do
    it "print missing option error and exit abnormal" do
      o, e, r = option("--aaa", "--ccc", "foo", "--bbb")

      o.should be_empty
      e.should eq(<<-EOS)
        option: missing option '--bbb'
        Try `option --help` for more information.

        EOS
      r.should eq(1)
    end
  end
  context "with invalid type option" do
    it "print invalid option error and exit abnormal" do
      o, e, r = option("--aaa", "--bbb", "foo", "--ccc", "foo")

      o.should be_empty
      e.should eq(<<-EOS)
        option: invalid option value 'foo' is not Int32
        Try `option --help` for more information.

        EOS
      r.should eq(1)
    end
  end
end
