require "../spec_helper"

{{ `crystal build -o bin/confirm examples/confirm.cr` }}

def confirm(*args, input = "")
  run("bin/confirm", args, input)
end

describe "examples/confirm" do
  context "with `y` answer" do
    it "print \"hello, world\" and exit normal" do
      o, e, r = confirm(input: "y")

      o.should eq(<<-EOS)
        Are you sure? [y/N]: hello, world

        EOS
      e.should be_empty
      r.should eq(0)
    end
  end

  context "with `n` answer" do
    it "print \"cancel\" and exit normal" do
      o, e, r = confirm(input: "n")

      o.should eq(<<-EOS)
        Are you sure? [y/N]: cancel

        EOS
      e.should be_empty
      r.should eq(0)
    end
  end

  context "with `-h` option" do
    it "print usage and exit normal" do
      o, e, r = confirm("-h")

      o.should eq(<<-EOS)
        Usage: confirm [option]

        Options:
            -v, --version                    print the version number and exit
            -h, --help                       print this help message and exit

        EOS
      e.should be_empty
      r.should eq(0)
    end
  end

  context "with `--help` option" do
    it "print usage and exit normal" do
      o, e, r = confirm("-h")

      o.should eq(<<-EOS)
        Usage: confirm [option]

        Options:
            -v, --version                    print the version number and exit
            -h, --help                       print this help message and exit

        EOS
      e.should be_empty
      r.should eq(0)
    end
  end

  context "with `-v` option" do
    it "print version and exit normal" do
      o, e, r = confirm("-v")

      o.should eq(<<-EOS)
        confirm #{CLIche::VERSION}

        EOS
      e.should be_empty
      r.should eq(0)
    end
  end

  context "with `--version` option" do
    it "print version and exit normal" do
      o, e, r = confirm("--version")

      o.should eq(<<-EOS)
        confirm #{CLIche::VERSION}

        EOS
      e.should be_empty
      r.should eq(0)
    end
  end

  context "with unrecognized option" do
    it "print unrecognized option error and exit abnormal" do
      o, e, r = confirm("-x")

      o.should be_empty
      e.should eq(<<-EOS)
        confirm: unrecognized option '-x'
        Try `confirm --help` for more information.

        EOS
      r.should eq(1)
    end
  end

  context "with invalid argument" do
    it "print invalid argument error and exit abnormal" do
      o, e, r = confirm("foo")

      o.should be_empty
      e.should eq(<<-EOS)
        confirm: invalid argument
        Try `confirm --help` for more information.

        EOS
      r.should eq(1)
    end
  end
end
