require "../spec_helper"

{{ `crystal build -o bin/basic examples/basic.cr` }}

def basic(*args)
  run("bin/basic", args)
end

describe "examples/basic" do
  context "without options and arguments" do
    it "print \"hello, world\" and exit normal" do
      o, e, r = basic

      o.should eq(<<-EOS)
        hello, world

        EOS
      e.should be_empty
      r.should eq(0)
    end
  end

  context "with `-h` option" do
    it "print usage and exit normal" do
      o, e, r = basic("-h")

      o.should eq(<<-EOS)
        Usage: basic [option]

        Options:
            -v, --version                    print the version number and exit
            -h, --help                       print this help message and exit

        EOS
      e.should be_empty
      r.should eq(0)
    end
  end

  context "with `--help` option" do
    it "print usage and exit normal" do
      o, e, r = basic("-h")

      o.should eq(<<-EOS)
        Usage: basic [option]

        Options:
            -v, --version                    print the version number and exit
            -h, --help                       print this help message and exit

        EOS
      e.should be_empty
      r.should eq(0)
    end
  end

  context "with `-v` option" do
    it "print version and exit normal" do
      o, e, r = basic("-v")

      o.should eq(<<-EOS)
        basic #{CLIche::VERSION}

        EOS
      e.should be_empty
      r.should eq(0)
    end
  end

  context "with `--version` option" do
    it "print version and exit normal" do
      o, e, r = basic("--version")

      o.should eq(<<-EOS)
        basic #{CLIche::VERSION}

        EOS
      e.should be_empty
      r.should eq(0)
    end
  end

  context "with unrecognized option" do
    it "print unrecognized option error and exit abnormal" do
      o, e, r = basic("-x")

      o.should be_empty
      e.should eq(<<-EOS)
        basic: unrecognized option '-x'
        Try `basic --help` for more information.

        EOS
      r.should eq(1)
    end
  end

  context "with invalid argument" do
    it "print invalid argument error and exit abnormal" do
      o, e, r = basic("foo")

      o.should be_empty
      e.should eq(<<-EOS)
        basic: invalid argument
        Try `basic --help` for more information.

        EOS
      r.should eq(1)
    end
  end
end
