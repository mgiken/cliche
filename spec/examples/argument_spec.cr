require "../spec_helper"

{{ `crystal build -o bin/argument examples/argument.cr` }}

def argument(*args)
  run("bin/argument", args)
end

describe "examples/argument" do
  context "with valid arguments" do
    it "print argument values and exit normal" do
      o, e, r = argument("foo", "bar")

      o.should eq(<<-EOS)
        foo = foo
        args = ["bar"]

        EOS
      e.should be_empty
      r.should eq(0)
    end
  end

  context "with `-h` option" do
    it "print usage and exit normal" do
      o, e, r = argument("-h")

      o.should eq(<<-EOS)
        Usage: argument [option] <foo> <args...>

        Options:
            -v, --version                    print the version number and exit
            -h, --help                       print this help message and exit

        EOS
      e.should be_empty
      r.should eq(0)
    end
  end

  context "with `--help` option" do
    it "print usage and exit normal" do
      o, e, r = argument("-h")

      o.should eq(<<-EOS)
        Usage: argument [option] <foo> <args...>

        Options:
            -v, --version                    print the version number and exit
            -h, --help                       print this help message and exit

        EOS
      e.should be_empty
      r.should eq(0)
    end
  end

  context "with `-v` option" do
    it "print version and exit normal" do
      o, e, r = argument("-v")

      o.should eq(<<-EOS)
        argument #{CLIche::VERSION}

        EOS
      e.should be_empty
      r.should eq(0)
    end
  end

  context "with `--version` option" do
    it "print version and exit normal" do
      o, e, r = argument("--version")

      o.should eq(<<-EOS)
        argument #{CLIche::VERSION}

        EOS
      e.should be_empty
      r.should eq(0)
    end
  end

  context "without arguments" do
    it "print missing argument error and exit abnormal" do
      o, e, r = argument

      o.should be_empty
      e.should eq(<<-EOS)
        argument: missing argument '<foo>'
        Try `argument --help` for more information.

        EOS
      r.should eq(1)
    end
  end

  context "without missing argument" do
    it "print missing argument error and exit abnormal" do
      o, e, r = argument("foo")

      o.should be_empty
      e.should eq(<<-EOS)
        argument: missing argument '<args>'
        Try `argument --help` for more information.

        EOS
      r.should eq(1)
    end
  end

  context "with unrecognized option" do
    it "print unrecognized option error and exit abnormal" do
      o, e, r = argument("-x")

      o.should be_empty
      e.should eq(<<-EOS)
        argument: unrecognized option '-x'
        Try `argument --help` for more information.

        EOS
      r.should eq(1)
    end
  end
end
