require "spec"
require "../src/cliche"

Colorize.enabled = true

def run(cmd, args = nil, input = "")
  a = [] of String
  args.each do |x|
    a << x.to_s
  end

  i = IO::Memory.new(input)
  o = IO::Memory.new
  e = IO::Memory.new
  s = Process.run(cmd, a, input: i, output: o, error: e)
  {o.to_s, e.to_s, s.exit_code}
end
