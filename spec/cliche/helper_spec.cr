require "../spec_helper"

class Foo
  extend CLIche::Helper
end

describe CLIche::Helper do
  describe "#echo" do
    it "print message" do
      io = IO::Memory.new
      Foo.echo("foo", "bar", io: io)
      io.to_s.should eq("    foo  bar\n")
    end
  end

  describe "#info" do
    it "print message with info color" do
      io = IO::Memory.new
      Foo.info("foo", "bar", io)
      io.to_s.should eq("    \e[37mfoo\e[0m  bar\n")
    end
  end

  describe "#success" do
    it "print message with success color" do
      io = IO::Memory.new
      Foo.success("foo", "bar", io)
      io.to_s.should eq("    \e[92mfoo\e[0m  bar\n")
    end
  end

  describe "#warning" do
    it "print message with warning color" do
      io = IO::Memory.new
      Foo.warning("foo", "bar", io)
      io.to_s.should eq("    \e[93mfoo\e[0m  bar\n")
    end
  end

  describe "#danger" do
    it "print message with danger color" do
      io = IO::Memory.new
      Foo.danger("foo", "bar", io)
      io.to_s.should eq("    \e[91mfoo\e[0m  bar\n")
    end
  end

  describe "#confirm" do
    context "when input is `y`" do
      it "print prompt and returns true" do
        o = IO::Memory.new
        i = IO::Memory.new("y")
        Foo.confirm("foo", io: o, input: i).should be_true
        o.to_s.should eq("foo [y/N]: ")
      end
    end

    context "when input is `n`" do
      it "print prompt and returns false" do
        o = IO::Memory.new
        i = IO::Memory.new("n")
        Foo.confirm("foo", io: o, input: i).should be_false
        o.to_s.should eq("foo [y/N]: ")
      end
    end

    context "when input is blank" do
      it "print prompt and returns false" do
        o = IO::Memory.new
        i = IO::Memory.new("")
        Foo.confirm("foo", io: o, input: i).should be_false
        o.to_s.should eq("foo [y/N]: ")
      end
    end

    context "when input is blank with custom prompt" do
      it "print custom prompt and returns false" do
        o = IO::Memory.new
        i = IO::Memory.new("")
        Foo.confirm("foo", "prompt", io: o, input: i).should be_false
        o.to_s.should eq("foo prompt ")
      end
    end
  end
end
