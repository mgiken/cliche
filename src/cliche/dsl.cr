require "colorize"

require "./command"

module CLIche::DSL
  macro included
    module CLI
      PROGNAME = File.basename(PROGRAM_NAME)

      def self.command
        @@command ||= {} of String => CLIche::Command
      end
    end
  end

  macro main
    module CLI
      VERSION  = \{{ `shards version #{__DIR__}`.stringify.chomp }}
    end

    class CLI::Main < CLIche::Command
      def_command(nil, {{ yield }})
    end

    CLI::Main.call(ARGV)
  end

  macro sub(name)
    class CLI::Sub{{ name.id.capitalize }} < CLIche::Command
      def_command({{ name }}, {{ yield }})
      CLI.command[{{ name.id.stringify }}] = new
    end
  end
end

include CLIche::DSL
