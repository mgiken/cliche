require "colorize"

module CLIche::Helper
  def echo(label, message, color = :default, io = STDOUT)
    io << "    "
    io << label.colorize(color)
    io << "  "
    io << message
    io << "\n"
    io.flush
  end

  def info(label, message, io = STDOUT)
    echo(label, message, :light_gray, io)
  end

  def success(label, message, io = STDOUT)
    echo(label, message, :light_green, io)
  end

  def warning(label, message, io = STDOUT)
    echo(label, message, :light_yellow, io)
  end

  def danger(label, message, io = STDOUT)
    echo(label, message, :light_red, io)
  end

  def confirm(message, prompt = "[y/N]:", io = STDOUT, input = STDIN)
    io << message
    io << " "
    io << prompt
    io << " "
    io.flush

    input.gets == "y"
  end
end
