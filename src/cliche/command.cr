require "option_parser"

require "./helper"

abstract class CLIche::Command
  include Helper

  getter name = ""
  getter description = ""
  getter option_parser = OptionParser.new

  def self.call(argv)
    new.call(argv)
  rescue e
    abort e.cause ? e.cause : e
  end

  abstract def call(args)

  def print_usage_and_exit
    puts(option_parser)
    exit
  end

  def print_version_and_exit
    puts("#{name} #{CLI::VERSION}")
    exit
  end

  def parse_abort(message, value = "")
    abort(<<-EOS)
      #{name}: #{message}#{value.empty? ? "" : " '#{value}'"}
      Try `#{name} --help` for more information.
      EOS
  end

  private macro def_command(name, expressions)
    {% main? = !name %}
    {% has_sub? = CLIche::Command.subclasses.size > 0 %}

    {% description = "" %}
    {% arguments = [] of Crystal::Macros::ASTNode %}
    {% options = [] of Crystal::Macros::ASTNode %}
    {% contents = [] of Crystal::Macros::ASTNode %}
    {% for exp in expressions.expressions %}
      {% if exp.id.starts_with?("description(") %}
        {% description = exp.args.first %}
      {% elsif exp.id.starts_with?("option(") %}
        {% options << exp.args %}
      {% elsif exp.id.starts_with?("arguments(") %}
        {% arguments = exp.args %}
      {% else %}
        {% contents << exp %}
      {% end %}
    {% end %}

    {% argument_keys = [] of String %}
    {% if main? && has_sub? %}
      {% argument_keys = ["<command>", "[<args>]"] %}
    {% end %}
    {% for a in arguments %}
      {% if a.class_name == "Splat" %}
        {% argument_keys << "<#{a.exp}...>" %}
      {% else %}
        {% argument_keys << "<#{a}>" %}
      {% end %}
    {% end %}

    def initialize
      {% if main? %}
        @name = CLI::PROGNAME
      {% else %}
        @name = "#{CLI::PROGNAME} {{ name.id }}"
      {% end %}
      @description = {{ description.id.stringify }}
    end

    protected def call(%argv)
      option_parser.banner = <<-EOS
        Usage: #{[name, "[option]", {{ argument_keys.splat }}].join(" ")}
      {% unless description.empty? %}
        {{ description[0..0].upcase.id }}{{ description[1..-1].id }}
      {% end %}
        Options:
        EOS

      option_parser.invalid_option do |x|
        parse_abort("unrecognized option", x)
      end

      option_parser.missing_option do |x|
        parse_abort("missing option", x)
      end

      {% for o in options %}
        {% node = o.first %}
        {% declare? = node.class_name == "TypeDeclaration" %}
        {% value = node.value %}
        {% var = declare? ? node.var : node.target %}
        {% type = declare? ? node.type.id : value.class_name.gsub(/Literal$/, "") %}

        {% args = o.last.split("|") %}
        {% for a, i in args %}
          {% unless i == args.size - 1 %}
            {% args[i] = (a.split(" ").first.size == 1 ? "-" : "--") + a %}
          {% end %}
        {% end %}

        {{ node }}
        option_parser.on({{ *args }}) do |x|
          {{ var }} = {% if type == "Bool" %}
            !{{ value }}
          {% elsif type == "Int32" %}
            x.to_i32
          {% elsif type == "Int64" %}
            x.to_i64
          {% elsif type == "Float32" %}
            x.to_f32
          {% elsif type == "Float64" %}
            x.to_f64
          {% elsif type == "Number" %}
            x.to_{{ value.kind.id }}
          {% else %}
            x
          {% end %}
        rescue %e : ArgumentError
          _, %t, %v = %e.message.to_s.split
          parse_abort("invalid option value '#{%v}' is not #{%t.chomp(':')}")
        end
      {% end %}

      {% if main? %}
        option_parser.on("-v", "--version", "print the version number and exit") do
          print_version_and_exit
        end
      {% end %}

      option_parser.on("-h", "--help", "print this help message and exit") do
        print_usage_and_exit
      end

      {% if main? && has_sub? %}
        option_parser.separator(<<-EOS)

          Commands:
          #{CLI.command.keys.sort.map { |k| "    #{k.ljust(33)}#{CLI.command[k].description}" }.join("\n")}

          For help on any individual command run `#{name} <command> --help`
          EOS

        cmd = %argv.shift?.to_s
        option_parser.parse([cmd])
        unless cmd.empty?
          parse_abort("unknown command", cmd) unless CLI.command.has_key?(cmd)
          return CLI.command[cmd].call(%argv)
        end
      {% else %}
        option_parser.parse(%argv)

        {% for a in arguments %}
          {% if a.class_name == "Splat" %}
            parse_abort("missing argument", "<{{ a.exp }}>") if %argv.empty?
            {{ a.exp }} = %argv.dup
            %argv.clear
          {% else %}
            parse_abort("missing argument", "<{{ a }}>") if %argv.empty?
            {{ a }} = %argv.shift
          {% end %}
        {% end %}

        parse_abort("invalid argument") unless %argv.empty?
      {% end %}

      {% for c in contents %}
        {{ c }}
      {% end %}
    end
  end
end
