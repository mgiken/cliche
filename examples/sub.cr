require "../src/cliche"

sub "foo" do
  description "sub foo description"

  puts "hello, sub foo"
end

main do
  description "main description"

  puts "hello, main"
end
