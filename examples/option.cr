require "../src/cliche"

main do
  option a : Bool = false, "a|aaa|AAA"
  option b = 1, "b BBB|bbb=BBB|BBB"
  option c = "foo", "c|ccc=CCC|CCC"
  option d = false, "d|DDD"
  option e = false, "eee|EEE"

  puts "a = #{a}"
  puts "b = #{b}"
  puts "c = #{c}"
  puts "d = #{d}"
  puts "e = #{e}"
end
